<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B02111">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King. A proclamation, for a publick general thanksgiving, throughout the realm of Scotland.</title>
    <author>Scotland. Sovereign (1649-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B02111 of text R173782 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C3311A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B02111</idno>
    <idno type="STC">Wing C3311A</idno>
    <idno type="STC">ESTC R173782</idno>
    <idno type="EEBO-CITATION">52612084</idno>
    <idno type="OCLC">ocm 52612084</idno>
    <idno type="VID">179367</idno>
    <idno type="PROQUESTGOID">2240865837</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B02111)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179367)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2786:28)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King. A proclamation, for a publick general thanksgiving, throughout the realm of Scotland.</title>
      <author>Scotland. Sovereign (1649-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by Evan Tyler, printer to the King's most excellent Majesty,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>1665.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Royal arms at head of text; initial letter.</note>
      <note>Printed in black letter.</note>
      <note>Dated at end: Given at Our Court at Whitehall, the tenth day of June, and of Our Reign the seventeenth year.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Public worship -- Scotland -- Early works to 1800.</term>
     <term>Church and state -- Scotland -- Early works to 1800.</term>
     <term>Anglo-Dutch War -- 1664-1667 -- Early works to 1800.</term>
     <term>Great Britain -- Foreign relations -- Netherlands -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. : A proclamation, for a publick general thanksgiving, throughout the realm of Scotland.</ep:title>
    <ep:author>Scotland. Sovereign </ep:author>
    <ep:publicationYear>1665</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>379</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-03</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B02111-t">
  <body xml:id="B02111-e0">
   <div type="royal_proclamation" xml:id="B02111-e10">
    <pb facs="tcp:179367:1" rend="simple:additions" xml:id="B02111-001-a"/>
    <head xml:id="B02111-e20">
     <figure xml:id="B02111-e30">
      <p xml:id="B02111-e40">
       <w lemma="c" pos="sy" xml:id="B02111-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="B02111-001-a-0020">R</w>
      </p>
      <p xml:id="B02111-e50">
       <w lemma="n/a" pos="ffr" xml:id="B02111-001-a-0030">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="B02111-001-a-0040">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="B02111-001-a-0050">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="B02111-001-a-0060">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="B02111-001-a-0070">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="B02111-001-a-0080">PENSE</w>
      </p>
      <figDesc xml:id="B02111-e60">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="B02111-e70">
     <w lemma="by" pos="acp" xml:id="B02111-001-a-0090">BY</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-0100">THE</w>
     <w lemma="king" pos="n1" xml:id="B02111-001-a-0110">KING</w>
     <pc unit="sentence" xml:id="B02111-001-a-0120">.</pc>
    </byline>
    <head xml:id="B02111-e80">
     <w lemma="a" pos="d" xml:id="B02111-001-a-0130">A</w>
     <w lemma="proclamation" pos="n1" xml:id="B02111-001-a-0140">PROCLAMATION</w>
     <pc xml:id="B02111-001-a-0150">,</pc>
     <w lemma="for" pos="acp" xml:id="B02111-001-a-0160">For</w>
     <w lemma="a" pos="d" xml:id="B02111-001-a-0170">a</w>
     <w lemma="public" pos="j" reg="Public" xml:id="B02111-001-a-0180">Publick</w>
     <w lemma="general" pos="n1" xml:id="B02111-001-a-0190">General</w>
     <w lemma="thanksgiving" pos="n1" xml:id="B02111-001-a-0200">Thanksgiving</w>
     <pc xml:id="B02111-001-a-0210">,</pc>
     <w lemma="throughout" pos="acp" xml:id="B02111-001-a-0220">throughout</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-0230">the</w>
     <w lemma="realm" pos="n1" xml:id="B02111-001-a-0240">Realm</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-0250">of</w>
     <w lemma="Scotland" pos="nn1" rend="hi" xml:id="B02111-001-a-0260">Scotland</w>
     <pc unit="sentence" xml:id="B02111-001-a-0270">.</pc>
    </head>
    <opener xml:id="B02111-e100">
     <signed xml:id="B02111-e110">
      <w lemma="charles" pos="nng1" reg="CHARLES'" rend="hi" xml:id="B02111-001-a-0280">CHARLES</w>
      <pc xml:id="B02111-001-a-0290">,</pc>
      <w lemma="by" pos="acp" xml:id="B02111-001-a-0300">by</w>
      <w lemma="the" pos="d" xml:id="B02111-001-a-0310">the</w>
      <w lemma="grace" pos="n1" xml:id="B02111-001-a-0320">grace</w>
      <w lemma="of" pos="acp" xml:id="B02111-001-a-0330">of</w>
      <w lemma="GOD" pos="nn1" xml:id="B02111-001-a-0340">GOD</w>
      <pc xml:id="B02111-001-a-0350">,</pc>
      <w lemma="king" pos="n1" xml:id="B02111-001-a-0360">King</w>
      <w lemma="of" pos="acp" xml:id="B02111-001-a-0370">of</w>
      <hi xml:id="B02111-e130">
       <w lemma="Scotland" pos="nn1" xml:id="B02111-001-a-0380">Scotland</w>
       <pc xml:id="B02111-001-a-0390">,</pc>
       <w lemma="England" pos="nn1" xml:id="B02111-001-a-0400">England</w>
       <pc xml:id="B02111-001-a-0410">,</pc>
       <w lemma="France" pos="nn1" xml:id="B02111-001-a-0420">France</w>
      </hi>
      <w lemma="and" pos="cc" xml:id="B02111-001-a-0430">and</w>
      <w lemma="Ireland" pos="nn1" rend="hi" xml:id="B02111-001-a-0440">Ireland</w>
      <pc xml:id="B02111-001-a-0450">,</pc>
      <w lemma="defender" pos="n1" xml:id="B02111-001-a-0460">Defender</w>
      <w lemma="of" pos="acp" xml:id="B02111-001-a-0470">of</w>
      <w lemma="the" pos="d" xml:id="B02111-001-a-0480">the</w>
      <w lemma="faith" pos="n1" xml:id="B02111-001-a-0490">Faith</w>
      <pc xml:id="B02111-001-a-0500">,</pc>
      <w lemma="etc." pos="ab" reg="etc." rend="hi" xml:id="B02111-001-a-0510">&amp;c.</w>
      <pc unit="sentence" xml:id="B02111-001-a-0520"/>
     </signed>
     <salute xml:id="B02111-e160">
      <w lemma="to" pos="prt" xml:id="B02111-001-a-0530">To</w>
      <w lemma="all" pos="d" xml:id="B02111-001-a-0540">all</w>
      <w lemma="and" pos="cc" xml:id="B02111-001-a-0550">and</w>
      <w lemma="sundry" pos="j" xml:id="B02111-001-a-0560">sundry</w>
      <w lemma="our" pos="po" xml:id="B02111-001-a-0570">Our</w>
      <w lemma="good" pos="j" xml:id="B02111-001-a-0580">good</w>
      <w lemma="subject" pos="n2" xml:id="B02111-001-a-0590">Subjects</w>
      <pc xml:id="B02111-001-a-0600">,</pc>
      <w lemma="greeting" pos="n1" xml:id="B02111-001-a-0610">Greeting</w>
      <pc xml:id="B02111-001-a-0620">;</pc>
     </salute>
    </opener>
    <p xml:id="B02111-e170">
     <w lemma="forasmuch" pos="av" xml:id="B02111-001-a-0630">Forasmuch</w>
     <w lemma="as" pos="acp" xml:id="B02111-001-a-0640">as</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-0650">Our</w>
     <w lemma="navy" pos="n1" xml:id="B02111-001-a-0660">Navy</w>
     <w lemma="royal" pos="j" xml:id="B02111-001-a-0670">Royal</w>
     <pc xml:id="B02111-001-a-0680">,</pc>
     <w lemma="under" pos="acp" xml:id="B02111-001-a-0690">under</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-0700">the</w>
     <w lemma="command" pos="n1" xml:id="B02111-001-a-0710">command</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-0720">of</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-0730">Our</w>
     <w lemma="dear" pos="js" xml:id="B02111-001-a-0740">dearest</w>
     <w lemma="brother" pos="n1" xml:id="B02111-001-a-0750">Brother</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-0760">the</w>
     <w lemma="duke" pos="n1" xml:id="B02111-001-a-0770">Duke</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-0780">of</w>
     <w lemma="York" pos="nn1" rend="hi" xml:id="B02111-001-a-0790">York</w>
     <pc xml:id="B02111-001-a-0800">,</pc>
     <w lemma="have" pos="vvz" xml:id="B02111-001-a-0810">hath</w>
     <pc xml:id="B02111-001-a-0820">,</pc>
     <w lemma="upon" pos="acp" xml:id="B02111-001-a-0830">upon</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-0840">the</w>
     <w lemma="three" pos="ord" xml:id="B02111-001-a-0850">third</w>
     <w lemma="day" pos="n1" xml:id="B02111-001-a-0860">day</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-0870">of</w>
     <w lemma="June" pos="nn1" rend="hi" xml:id="B02111-001-a-0880">June</w>
     <w lemma="last" pos="ord" xml:id="B02111-001-a-0890">last</w>
     <pc xml:id="B02111-001-a-0900">,</pc>
     <w lemma="obtain" pos="vvd" xml:id="B02111-001-a-0910">obtained</w>
     <w lemma="a" pos="d" xml:id="B02111-001-a-0920">a</w>
     <w lemma="glorious" pos="j" xml:id="B02111-001-a-0930">glorious</w>
     <w lemma="victory" pos="n1" xml:id="B02111-001-a-0940">Victory</w>
     <w lemma="over" pos="acp" xml:id="B02111-001-a-0950">over</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-0960">the</w>
     <w lemma="fleet" pos="n1" xml:id="B02111-001-a-0970">Fleet</w>
     <w lemma="set" pos="vvd" xml:id="B02111-001-a-0980">set</w>
     <w lemma="out" pos="av" xml:id="B02111-001-a-0990">out</w>
     <w lemma="by" pos="acp" xml:id="B02111-001-a-1000">by</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-1010">the</w>
     <w lemma="state" pos="n2" xml:id="B02111-001-a-1020">States</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-1030">of</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-1040">the</w>
     <hi xml:id="B02111-e200">
      <w lemma="unite" pos="j-vn" xml:id="B02111-001-a-1050">United</w>
      <w lemma="province" pos="n2" xml:id="B02111-001-a-1060">Provinces</w>
     </hi>
     <pc rend="follows-hi" xml:id="B02111-001-a-1070">:</pc>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-1080">And</w>
     <w lemma="we" pos="pns" xml:id="B02111-001-a-1090">We</w>
     <w lemma="find" pos="vvg" xml:id="B02111-001-a-1100">finding</w>
     <w lemma="it" pos="pn" xml:id="B02111-001-a-1110">it</w>
     <w lemma="suitable" pos="j" reg="suitable" xml:id="B02111-001-a-1120">suteable</w>
     <pc xml:id="B02111-001-a-1130">,</pc>
     <w lemma="that" pos="cs" xml:id="B02111-001-a-1140">that</w>
     <w lemma="a" pos="d" xml:id="B02111-001-a-1150">a</w>
     <w lemma="solemn" pos="j" xml:id="B02111-001-a-1160">solemn</w>
     <w lemma="return" pos="n1" xml:id="B02111-001-a-1170">return</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-1180">of</w>
     <w lemma="praise" pos="n1" xml:id="B02111-001-a-1190">Praise</w>
     <w lemma="be" pos="vvb" xml:id="B02111-001-a-1200">be</w>
     <w lemma="pay" pos="vvn" xml:id="B02111-001-a-1210">paid</w>
     <w lemma="to" pos="acp" xml:id="B02111-001-a-1220">to</w>
     <w lemma="almighty" pos="j" xml:id="B02111-001-a-1230">Almighty</w>
     <w lemma="GOD" pos="nn1" xml:id="B02111-001-a-1240">GOD</w>
     <pc xml:id="B02111-001-a-1250">,</pc>
     <w lemma="by" pos="acp" xml:id="B02111-001-a-1260">by</w>
     <w lemma="who" pos="crq" xml:id="B02111-001-a-1270">whose</w>
     <w lemma="special" pos="j" xml:id="B02111-001-a-1280">special</w>
     <w lemma="hand" pos="n1" xml:id="B02111-001-a-1290">Hand</w>
     <pc xml:id="B02111-001-a-1300">,</pc>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-1310">and</w>
     <w lemma="signal" pos="n1" xml:id="B02111-001-a-1320">signal</w>
     <w lemma="appearance" pos="n1" xml:id="B02111-001-a-1330">Appearance</w>
     <w lemma="for" pos="acp" xml:id="B02111-001-a-1340">for</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="B02111-001-a-1350">Vs</w>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-1360">and</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-1370">the</w>
     <w lemma="justice" pos="n1" xml:id="B02111-001-a-1380">justice</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-1390">of</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-1400">Our</w>
     <w lemma="cause" pos="n1" xml:id="B02111-001-a-1410">Cause</w>
     <pc xml:id="B02111-001-a-1420">,</pc>
     <w lemma="this" pos="d" xml:id="B02111-001-a-1430">this</w>
     <w lemma="great" pos="j" xml:id="B02111-001-a-1440">great</w>
     <w lemma="salvation" pos="n1" xml:id="B02111-001-a-1450">Salvation</w>
     <w lemma="have" pos="vvz" xml:id="B02111-001-a-1460">hath</w>
     <w lemma="be" pos="vvn" xml:id="B02111-001-a-1470">been</w>
     <w lemma="wrought" pos="vvn" xml:id="B02111-001-a-1480">wrought</w>
     <pc xml:id="B02111-001-a-1490">;</pc>
     <w lemma="have" pos="vvb" xml:id="B02111-001-a-1500">Have</w>
     <w lemma="judge" pos="vvn" xml:id="B02111-001-a-1510">judged</w>
     <w lemma="fit" pos="j" xml:id="B02111-001-a-1520">fit</w>
     <pc xml:id="B02111-001-a-1530">,</pc>
     <w lemma="by" pos="acp" xml:id="B02111-001-a-1540">by</w>
     <w lemma="this" pos="d" xml:id="B02111-001-a-1550">this</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-1560">Our</w>
     <w lemma="proclamation" pos="n1" xml:id="B02111-001-a-1570">Proclamation</w>
     <pc xml:id="B02111-001-a-1580">,</pc>
     <w lemma="to" pos="prt" xml:id="B02111-001-a-1590">to</w>
     <w lemma="indict" pos="vvi" xml:id="B02111-001-a-1600">indict</w>
     <w lemma="a" pos="d" xml:id="B02111-001-a-1610">a</w>
     <w lemma="general" pos="j" xml:id="B02111-001-a-1620">general</w>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-1630">and</w>
     <w lemma="public" pos="j" reg="public" xml:id="B02111-001-a-1640">publick</w>
     <w lemma="thanksgiving" pos="n1" xml:id="B02111-001-a-1650">Thanksgiving</w>
     <w lemma="for" pos="acp" xml:id="B02111-001-a-1660">for</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-1670">the</w>
     <w lemma="cause" pos="n1" xml:id="B02111-001-a-1680">cause</w>
     <w lemma="aforesaid" pos="j" xml:id="B02111-001-a-1690">aforesaid</w>
     <pc unit="sentence" xml:id="B02111-001-a-1700">.</pc>
     <w lemma="our" pos="po" xml:id="B02111-001-a-1710">Our</w>
     <w lemma="will" pos="n1" xml:id="B02111-001-a-1720">will</w>
     <w lemma="be" pos="vvz" xml:id="B02111-001-a-1730">is</w>
     <w lemma="herefore" pos="av" xml:id="B02111-001-a-1740">herefore</w>
     <pc xml:id="B02111-001-a-1750">,</pc>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-1760">and</w>
     <w lemma="we" pos="pns" xml:id="B02111-001-a-1770">We</w>
     <w lemma="strait" pos="av-j" reg="straight" xml:id="B02111-001-a-1780">straitly</w>
     <w lemma="command" pos="vvi" xml:id="B02111-001-a-1790">command</w>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-1800">and</w>
     <w lemma="charge" pos="n1" xml:id="B02111-001-a-1810">charge</w>
     <pc xml:id="B02111-001-a-1820">,</pc>
     <w lemma="that" pos="cs" xml:id="B02111-001-a-1830">that</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-1840">the</w>
     <w lemma="say" pos="j-vn" xml:id="B02111-001-a-1850">said</w>
     <w lemma="thanksgiving" pos="n1" xml:id="B02111-001-a-1860">Thanksgiving</w>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-1870">and</w>
     <w lemma="solemn" pos="j" xml:id="B02111-001-a-1880">solemn</w>
     <w lemma="commemoration" pos="n1" xml:id="B02111-001-a-1890">Commemoration</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-1900">of</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-1910">the</w>
     <w lemma="goodness" pos="n1" xml:id="B02111-001-a-1920">goodness</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-1930">of</w>
     <w lemma="GOD" pos="nn1" xml:id="B02111-001-a-1940">GOD</w>
     <pc xml:id="B02111-001-a-1950">,</pc>
     <w lemma="manifest" pos="vvn" xml:id="B02111-001-a-1960">manifested</w>
     <w lemma="by" pos="acp" xml:id="B02111-001-a-1970">by</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-1980">the</w>
     <w lemma="conduct" pos="n1" xml:id="B02111-001-a-1990">conduct</w>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-2000">and</w>
     <w lemma="management" pos="n1" xml:id="B02111-001-a-2010">management</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-2020">of</w>
     <w lemma="this" pos="d" xml:id="B02111-001-a-2030">this</w>
     <w lemma="late" pos="j" xml:id="B02111-001-a-2040">late</w>
     <w lemma="action" pos="n1" xml:id="B02111-001-a-2050">Action</w>
     <pc xml:id="B02111-001-a-2060">,</pc>
     <w lemma="be" pos="vvb" xml:id="B02111-001-a-2070">be</w>
     <w lemma="religious" pos="av-j" xml:id="B02111-001-a-2080">religiously</w>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-2090">and</w>
     <w lemma="solemn" pos="av-j" xml:id="B02111-001-a-2100">solemnly</w>
     <w lemma="observe" pos="vvn" xml:id="B02111-001-a-2110">observed</w>
     <w lemma="through" pos="acp" xml:id="B02111-001-a-2120">through</w>
     <w lemma="this" pos="d" xml:id="B02111-001-a-2130">this</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-2140">Our</w>
     <w lemma="whole" pos="j" xml:id="B02111-001-a-2150">whole</w>
     <w lemma="kingdom" pos="n1" xml:id="B02111-001-a-2160">Kingdom</w>
     <pc xml:id="B02111-001-a-2170">,</pc>
     <w lemma="upon" pos="acp" xml:id="B02111-001-a-2180">upon</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-2190">the</w>
     <w lemma="second" pos="ord" xml:id="B02111-001-a-2200">second</w>
     <w lemma="Thursday" pos="nn1" xml:id="B02111-001-a-2210">Thursday</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-2220">of</w>
     <w lemma="July" pos="nn1" rend="hi" xml:id="B02111-001-a-2230">July</w>
     <w lemma="next" pos="ord" xml:id="B02111-001-a-2240">next</w>
     <pc xml:id="B02111-001-a-2250">,</pc>
     <w lemma="be" pos="vvg" xml:id="B02111-001-a-2260">being</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-2270">the</w>
     <w lemma="thirteen" pos="ord" xml:id="B02111-001-a-2280">thirteenth</w>
     <w lemma="day" pos="n1" xml:id="B02111-001-a-2290">day</w>
     <w lemma="thereof" pos="av" xml:id="B02111-001-a-2300">thereof</w>
     <pc xml:id="B02111-001-a-2310">;</pc>
     <w lemma="require" pos="vvg" xml:id="B02111-001-a-2320">Requiring</w>
     <w lemma="hereby" pos="av" xml:id="B02111-001-a-2330">hereby</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-2340">Our</w>
     <w lemma="reverend" pos="j" xml:id="B02111-001-a-2350">Reverend</w>
     <w lemma="archbishop" pos="n2" xml:id="B02111-001-a-2360">Archbishops</w>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-2370">and</w>
     <w lemma="bishop" pos="n2" xml:id="B02111-001-a-2380">Bishops</w>
     <pc xml:id="B02111-001-a-2390">,</pc>
     <w lemma="to" pos="prt" xml:id="B02111-001-a-2400">to</w>
     <w lemma="give" pos="vvi" xml:id="B02111-001-a-2410">give</w>
     <w lemma="notice" pos="n1" xml:id="B02111-001-a-2420">notice</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-2430">of</w>
     <w lemma="this" pos="d" xml:id="B02111-001-a-2440">this</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-2450">Our</w>
     <w lemma="royal" pos="j" xml:id="B02111-001-a-2460">Royal</w>
     <w lemma="pleasure" pos="n1" xml:id="B02111-001-a-2470">Pleasure</w>
     <w lemma="to" pos="acp" xml:id="B02111-001-a-2480">to</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-2490">the</w>
     <w lemma="minister" pos="n2" xml:id="B02111-001-a-2500">Ministers</w>
     <w lemma="in" pos="acp" xml:id="B02111-001-a-2510">in</w>
     <w lemma="their" pos="po" xml:id="B02111-001-a-2520">their</w>
     <w lemma="respective" pos="j" xml:id="B02111-001-a-2530">respective</w>
     <w lemma="diocese" pos="n2" reg="Dioceses" xml:id="B02111-001-a-2540">Diocesses</w>
     <pc xml:id="B02111-001-a-2550">;</pc>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-2560">and</w>
     <w lemma="that" pos="cs" xml:id="B02111-001-a-2570">that</w>
     <w lemma="upon" pos="acp" xml:id="B02111-001-a-2580">upon</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-2590">the</w>
     <w lemma="lordsday" pos="n1" reg="Lordsday" xml:id="B02111-001-a-2600">Lords-day</w>
     <w lemma="immediate" pos="av-j" reg="immediately" xml:id="B02111-001-a-2610">immediatly</w>
     <w lemma="precede" pos="vvg" reg="preceding" xml:id="B02111-001-a-2620">preceeding</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-2630">the</w>
     <w lemma="say" pos="vvn" xml:id="B02111-001-a-2640">said</w>
     <w lemma="thirteen" pos="ord" xml:id="B02111-001-a-2650">thirteenth</w>
     <w lemma="day" pos="n1" xml:id="B02111-001-a-2660">day</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-2670">of</w>
     <w lemma="July" pos="nn1" rend="hi" xml:id="B02111-001-a-2680">July</w>
     <pc xml:id="B02111-001-a-2690">,</pc>
     <w lemma="they" pos="pns" xml:id="B02111-001-a-2700">they</w>
     <w lemma="cause" pos="vvb" xml:id="B02111-001-a-2710">cause</w>
     <w lemma="read" pos="vvn" xml:id="B02111-001-a-2720">read</w>
     <w lemma="this" pos="d" xml:id="B02111-001-a-2730">this</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-2740">Our</w>
     <w lemma="proclamation" pos="n1" xml:id="B02111-001-a-2750">Proclamation</w>
     <w lemma="from" pos="acp" xml:id="B02111-001-a-2760">from</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-2770">the</w>
     <w lemma="pulpit" pos="n1" xml:id="B02111-001-a-2780">Pulpit</w>
     <w lemma="in" pos="acp" xml:id="B02111-001-a-2790">in</w>
     <w lemma="every" pos="d" xml:id="B02111-001-a-2800">every</w>
     <w lemma="paroch" pos="n1" xml:id="B02111-001-a-2810">Paroch</w>
     <w lemma="kirk" pos="n1" xml:id="B02111-001-a-2820">Kirk</w>
     <pc xml:id="B02111-001-a-2830">:</pc>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-2840">And</w>
     <w lemma="that" pos="cs" xml:id="B02111-001-a-2850">that</w>
     <w lemma="they" pos="pns" xml:id="B02111-001-a-2860">they</w>
     <w lemma="exhort" pos="vvb" xml:id="B02111-001-a-2870">exhort</w>
     <w lemma="all" pos="d" xml:id="B02111-001-a-2880">all</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-2890">Our</w>
     <w lemma="love" pos="j-vg" xml:id="B02111-001-a-2900">loving</w>
     <w lemma="subject" pos="n2" xml:id="B02111-001-a-2910">Subjects</w>
     <w lemma="to" pos="acp" xml:id="B02111-001-a-2920">to</w>
     <w lemma="a" pos="d" xml:id="B02111-001-a-2930">a</w>
     <w lemma="cheerful" pos="j" reg="cheerful" xml:id="B02111-001-a-2940">chearfull</w>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-2950">and</w>
     <w lemma="dévout" pos="j" xml:id="B02111-001-a-2960">dévout</w>
     <w lemma="performance" pos="n1" xml:id="B02111-001-a-2970">performance</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-2980">of</w>
     <w lemma="this" pos="d" xml:id="B02111-001-a-2990">this</w>
     <w lemma="so" pos="av" xml:id="B02111-001-a-3000">so</w>
     <w lemma="become" pos="vvg" xml:id="B02111-001-a-3010">becoming</w>
     <w lemma="a" pos="d" xml:id="B02111-001-a-3020">a</w>
     <w lemma="duty" pos="n1" xml:id="B02111-001-a-3030">duty</w>
     <w lemma="they" pos="pns" xml:id="B02111-001-a-3040">they</w>
     <w lemma="owe" pos="vvb" xml:id="B02111-001-a-3050">owe</w>
     <w lemma="to" pos="acp" xml:id="B02111-001-a-3060">to</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-3070">the</w>
     <w lemma="name" pos="n1" xml:id="B02111-001-a-3080">Name</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-3090">of</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-3100">the</w>
     <w lemma="lord" pos="n1" xml:id="B02111-001-a-3110">LORD</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-3120">Our</w>
     <w lemma="GOD" pos="nn1" xml:id="B02111-001-a-3130">GOD</w>
     <pc xml:id="B02111-001-a-3140">,</pc>
     <w lemma="who" pos="crq" xml:id="B02111-001-a-3150">who</w>
     <w lemma="have" pos="vvz" xml:id="B02111-001-a-3160">has</w>
     <w lemma="do" pos="vvn" xml:id="B02111-001-a-3170">done</w>
     <w lemma="these" pos="d" xml:id="B02111-001-a-3180">these</w>
     <w lemma="great" pos="j" xml:id="B02111-001-a-3190">great</w>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-3200">and</w>
     <w lemma="auspicious" pos="j" xml:id="B02111-001-a-3210">auspicious</w>
     <w lemma="thing" pos="n2" xml:id="B02111-001-a-3220">things</w>
     <w lemma="for" pos="acp" xml:id="B02111-001-a-3230">for</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="B02111-001-a-3240">Vs</w>
     <pc xml:id="B02111-001-a-3250">,</pc>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-3260">and</w>
     <w lemma="for" pos="acp" xml:id="B02111-001-a-3270">for</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-3280">the</w>
     <w lemma="honour" pos="n1" xml:id="B02111-001-a-3290">Honour</w>
     <w lemma="and" pos="cc" xml:id="B02111-001-a-3300">and</w>
     <w lemma="interest" pos="n1" xml:id="B02111-001-a-3310">Interest</w>
     <w lemma="of" pos="acp" xml:id="B02111-001-a-3320">of</w>
     <w lemma="our" pos="po" xml:id="B02111-001-a-3330">Our</w>
     <w lemma="kingdom" pos="n2" xml:id="B02111-001-a-3340">Kingdoms</w>
     <pc unit="sentence" xml:id="B02111-001-a-3350">.</pc>
    </p>
    <closer xml:id="B02111-e230">
     <dateline xml:id="B02111-e240">
      <w lemma="give" pos="vvn" xml:id="B02111-001-a-3360">Given</w>
      <w lemma="at" pos="acp" xml:id="B02111-001-a-3370">at</w>
      <w lemma="our" pos="po" xml:id="B02111-001-a-3380">Our</w>
      <w lemma="court" pos="n1" xml:id="B02111-001-a-3390">Court</w>
      <w lemma="at" pos="acp" xml:id="B02111-001-a-3400">at</w>
      <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="B02111-001-a-3410">Whitehall</w>
      <pc xml:id="B02111-001-a-3420">,</pc>
      <date xml:id="B02111-e260">
       <w lemma="the" pos="d" xml:id="B02111-001-a-3430">the</w>
       <w lemma="ten" pos="ord" xml:id="B02111-001-a-3440">tenth</w>
       <w lemma="day" pos="n1" xml:id="B02111-001-a-3450">day</w>
       <w lemma="of" pos="acp" xml:id="B02111-001-a-3460">of</w>
       <w lemma="June" pos="nn1" rend="hi" xml:id="B02111-001-a-3470">June</w>
       <pc xml:id="B02111-001-a-3480">,</pc>
       <w lemma="and" pos="cc" xml:id="B02111-001-a-3490">and</w>
       <w lemma="of" pos="acp" xml:id="B02111-001-a-3500">of</w>
       <w lemma="our" pos="po" xml:id="B02111-001-a-3510">Our</w>
       <w lemma="reign" pos="n1" xml:id="B02111-001-a-3520">Reign</w>
       <w lemma="the" pos="d" xml:id="B02111-001-a-3530">the</w>
       <w lemma="seventeen" pos="ord" xml:id="B02111-001-a-3540">seventeenth</w>
       <w lemma="year" pos="n1" xml:id="B02111-001-a-3550">year</w>
       <pc unit="sentence" xml:id="B02111-001-a-3560">.</pc>
      </date>
     </dateline>
     <lb xml:id="B02111-e280"/>
     <w lemma="GOD" pos="nn1" xml:id="B02111-001-a-3570">GOD</w>
     <w lemma="save" pos="acp" xml:id="B02111-001-a-3580">SAVE</w>
     <w lemma="the" pos="d" xml:id="B02111-001-a-3590">THE</w>
     <w lemma="king" pos="n1" xml:id="B02111-001-a-3600">KING</w>
     <pc unit="sentence" xml:id="B02111-001-a-3610">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="B02111-e290">
   <div type="colophon" xml:id="B02111-e300">
    <p xml:id="B02111-e310">
     <hi xml:id="B02111-e320">
      <w lemma="Edinburgh" pos="nn1" xml:id="B02111-001-a-3620">Edinburgh</w>
      <pc xml:id="B02111-001-a-3630">,</pc>
      <hi xml:id="B02111-e330">
       <w lemma="print" pos="vvn" xml:id="B02111-001-a-3640">Printed</w>
       <w lemma="by" pos="acp" xml:id="B02111-001-a-3650">by</w>
      </hi>
      <w lemma="Evan" pos="nn1" xml:id="B02111-001-a-3660">Evan</w>
      <w lemma="Tyler" pos="nn1" xml:id="B02111-001-a-3670">Tyler</w>
      <pc xml:id="B02111-001-a-3680">,</pc>
      <hi xml:id="B02111-e340">
       <w lemma="printer" pos="n1" xml:id="B02111-001-a-3690">Printer</w>
       <w lemma="to" pos="acp" xml:id="B02111-001-a-3700">to</w>
       <w lemma="the" pos="d" xml:id="B02111-001-a-3710">the</w>
       <w join="right" lemma="king" pos="n1" xml:id="B02111-001-a-3720">King</w>
       <w join="left" lemma="be" pos="vvz" xml:id="B02111-001-a-3721">'s</w>
       <w lemma="most" pos="avs-d" xml:id="B02111-001-a-3730">most</w>
       <w lemma="excellent" pos="j" xml:id="B02111-001-a-3740">Excellent</w>
       <w lemma="majesty" pos="n1" xml:id="B02111-001-a-3750">Majesty</w>
       <pc xml:id="B02111-001-a-3760">,</pc>
       <w lemma="1665." pos="crd" xml:id="B02111-001-a-3770">1665.</w>
       <pc unit="sentence" xml:id="B02111-001-a-3780"/>
      </hi>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
